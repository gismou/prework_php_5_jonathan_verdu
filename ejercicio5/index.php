<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>
<body>
    
    <?php

        $path = "./";
        $dir = opendir($path);

        $crear_carpeta = true;
        $fechaActual = date('d-m-Y H:i:s');
        $fechaActual = str_replace(" ","_",$fechaActual);
        $fechaActual = str_replace(":","_",$fechaActual);
        $ruta_nueva_carpeta = $path."/".$fechaActual;
        while ($elemento = readdir($dir)){
            if( $elemento != "." && $elemento != ".."){
                if($crear_carpeta){
                    //crear carpeta
                    mkdir($ruta_nueva_carpeta, 0700, true);
                    $crear_carpeta = false;
                }
                if( !is_dir($path.$elemento) ){
                    //crear copia del fichero en nueva carpeta
                    copy($elemento,$ruta_nueva_carpeta."/".$elemento.".modificado");
                }
            }
        }
  

    ?>

</body>
</html>
